-- Post-require: lib/tweak_data/skilltreetweakdata
-- Fix for Martial Arts Aced not working

local _SkillTreeTweakData_init = SkillTreeTweakData.init

function SkillTreeTweakData:init()
    _SkillTreeTweakData_init(self)
    
    self.skills.martial_arts[2].upgrades[1] = "player_melee_knockdown_mul_1"
end