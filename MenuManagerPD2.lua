-- Post-require: lib/managers/menumanagerpd2
-- Equips a random mask for every heist (Coop)
-- U101: Untested, just sayin'

-- The number of masks to use when randomizing (set to 1000 or so to use all)
-- Counted based on your inventory from left to right, then top to bottom, starting with the default mask
local MASKS_TO_RANDOMIZE = 1000

-- Do not change anything past this point unless you know what you are doing
local _MenuManager_on_enter_lobby = MenuManager.on_enter_lobby

function MenuManager:on_enter_lobby()
    local masks_data = Global.blackmarket_manager.crafted_items["masks"]
    if masks_data then
        local array = {}
        local counter = 0
        local counter_shift = 0
        for s, data in pairs(masks_data) do
            if counter >= MASKS_TO_RANDOMIZE then
                break
            end
            counter = counter + 1
            if data.equipped then
                counter_shift = -1
            else
                array[counter + counter_shift] = s
            end
        end

        local index = math.random(counter + counter_shift)
        local slot = array[index]
        managers.blackmarket:equip_mask(slot)
    end

    _MenuManager_on_enter_lobby(self)
end