-- Post-require: lib/tweak_data/upgradestweakdata
-- Sharpshooter Aced gives the +8 (instead of +4) stability that the skill's description states
-- Yes, it is literally only ONE NUMBER to be changed...

local _UpgradesTweakData_init = UpgradesTweakData.init

function UpgradesTweakData:init()
    _UpgradesTweakData_init(self)
    
    -- Technician AR Recoil Dampener
    self.values.assault_rifle.recoil_index_addend = {2}
end