-- Post-require: lib/managers/blackmarketmanager
-- Fix for Marksman Aced making accuracy worse

local _BlackMarketManager_accuracy_addend = BlackMarketManager.accuracy_addend

function BlackMarketManager:accuracy_addend(name, category, sub_category, spread_index, silencer, current_state, fire_mode, blueprint, is_moving, is_single_shot)
    local addend = _BlackMarketManager_accuracy_addend(self, name, category, sub_category, spread_index, silencer, current_state, fire_mode, blueprint, is_moving, is_single_shot)
    if addend > 0 and managers.player:has_category_upgrade("player", "single_shot_accuracy_inc") and current_state and current_state:in_steelsight() and is_single_shot then
        addend = addend / managers.player:upgrade_value("player", "single_shot_accuracy_inc", 1)
        addend = addend * ((1 - managers.player:upgrade_value("player", "single_shot_accuracy_inc", 1)) + 1)
    end
    return addend
end