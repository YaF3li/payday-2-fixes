-- Post-require: lib/units/weapons/shotgun/newshotgunbase
-- Fix for AP slugs and DB rounds not piercing shields reliably

local mvec_to = Vector3()
local mvec_direction = Vector3()
local mvec_spread_direction = Vector3()

local _NewShotgunBase__update_stats_values = NewShotgunBase._update_stats_values
function NewShotgunBase:_update_stats_values()
    _NewShotgunBase__update_stats_values(self)
    self._can_shoot_through_shield = self._ammo_data and self._ammo_data.can_shoot_through_shield
    self._bullet_slotmask_no_shields = self._bullet_slotmask - 8
end

function NewShotgunBase:_fire_raycast(user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
    -- Redirect whole call to RaycastWeaponBase for single-bullet ammo types, just like it was pre-BBQ update
    if self._rays == 1 then
        local result = NewShotgunBase.super._fire_raycast(self, user_unit, from_pos, direction, dmg_mul, shoot_player, spread_mul, autohit_mul, suppr_mul, shoot_through_data)
        return result
    end
    local result = {}
    local hit_enemies = {}
    local hit_something, col_rays
    if self._alert_events then
        col_rays = {}
    end
    local damage = self:_get_current_damage(dmg_mul)
    local autoaim, dodge_enemies = self:check_autoaim(from_pos, direction, self._range)
    local weight = 0.1
    local enemy_died = false
    local function hit_enemy(col_ray)
        if col_ray.unit:character_damage() then
            local enemy_key = col_ray.unit:key()
            if not hit_enemies[enemy_key] or col_ray.unit:character_damage().is_head and col_ray.unit:character_damage():is_head(col_ray.body) then
                hit_enemies[enemy_key] = col_ray
            end
        else
            self._bullet_class:on_collision(col_ray, self._unit, user_unit, damage)
        end
    end

    local spread = self:_get_spread(user_unit)
    mvector3.set(mvec_direction, direction)
    for i = 1, self._rays do
        mvector3.set(mvec_spread_direction, mvec_direction)
        if spread then
            mvector3.spread(mvec_spread_direction, spread * (spread_mul or 1))
        end
        mvector3.set(mvec_to, mvec_spread_direction)
        mvector3.multiply(mvec_to, 20000)
        mvector3.add(mvec_to, from_pos)
        local col_ray = World:raycast("ray", from_pos, mvec_to, "slot_mask", self._bullet_slotmask, "ignore_unit", self._setup.ignore_units)
        
        -- Have NewShotgunBase handle shield penetration for DB by itself to account for multi-pellet nature of DB rounds
        -- First, see if we are generally able to shoot through shields and if we actually hit one with our current pellet
        if self._can_shoot_through_shield and col_ray and col_ray.unit:in_slot(8) then
            -- Do the raycast again, but with a slotmask that will not collide (i. e. ignore) shield plates
            local col_ray2 = World:raycast("ray", from_pos, mvec_to, "slot_mask", self._bullet_slotmask_no_shields, "ignore_unit", self._setup.ignore_units)
            -- If the second raycast supplied a result, use that instead of the original one
            -- And make sure we remember that this pellet went through a shield
            if col_ray2 then
                col_ray = col_ray2
                col_ray.has_penetrated_shield = true
            end
        end
        
        if col_rays then
            if col_ray then
                table.insert(col_rays, col_ray)
            else
                local ray_to = mvector3.copy(mvec_to)
                local spread_direction = mvector3.copy(mvec_spread_direction)
                table.insert(col_rays, {position = ray_to, ray = spread_direction})
            end
        end
        if self._autoaim and autoaim then
            if col_ray and col_ray.unit:in_slot(managers.slot:get_mask("enemies")) then
                self._autohit_current = (self._autohit_current + weight) / (1 + weight)
                hit_enemy(col_ray)
                autoaim = false
            else
                autoaim = false
                local autohit = self:check_autoaim(from_pos, direction, self._range)
                if autohit then
                    local autohit_chance = 1 - math.clamp((self._autohit_current - self._autohit_data.MIN_RATIO) / (self._autohit_data.MAX_RATIO - self._autohit_data.MIN_RATIO), 0, 1)
                    if autohit_chance > math.random() then
                        self._autohit_current = (self._autohit_current + weight) / (1 + weight)
                        hit_something = true
                        hit_enemy(autohit)
                    else
                        self._autohit_current = self._autohit_current / (1 + weight)
                    end
                elseif col_ray then
                    hit_something = true
                    hit_enemy(col_ray)
                end
            end
        elseif col_ray then
            hit_something = true
            hit_enemy(col_ray)
        end
    end
    for _, col_ray in pairs(hit_enemies) do
        local udamage = self:get_damage_falloff(damage, col_ray, user_unit)
        if udamage > 0 then
            -- Account for damage reduction when penetrating shields
            if col_ray.has_penetrated_shield then
                udamage = udamage * 0.25
            end
            local result = self._bullet_class:on_collision(col_ray, self._unit, user_unit, udamage)
            if result and result.type == "death" then
                managers.game_play_central:do_shotgun_push(col_ray.unit, col_ray.position, col_ray.ray, col_ray.distance)
            end
        end
    end
    if dodge_enemies and self._suppression then
        for enemy_data, dis_error in pairs(dodge_enemies) do
            enemy_data.unit:character_damage():build_suppression(suppr_mul * dis_error * self._suppression, self._panic_suppression_chance)
        end
    end
    result.hit_enemy = next(hit_enemies) and true or false
    if self._alert_events then
        result.rays = #col_rays > 0 and col_rays
    end
    managers.statistics:shot_fired({
        hit = false,
        weapon_unit = self._unit
    })
    for _, _ in pairs(hit_enemies) do
        managers.statistics:shot_fired({
            hit = true,
            weapon_unit = self._unit,
            skip_bullet_count = true
        })
    end
    return result
end