-- Post-require: lib/units/beings/player/playerdamage
-- Ex-President keeps left-over health you did not use for healing in the store instead of discarding it
-- WARNING: This is obviously a gameplay altering mod and if you do not like that, do not use this!

function PlayerDamage:consume_armor_stored_health(amount)
    if self._armor_stored_health and not self._dead and not self._bleed_out and not self._check_berserker_done then
        -- Remember health from before, so we can calculate how much health we actually gained
        local health_added = self:get_real_health()
        self:change_health(self._armor_stored_health)
        health_added = self:get_real_health() - health_added
        -- Set stored health to the amount that we did not use up for the heal
        self._armor_stored_health = math.max(self._armor_stored_health - health_added, 0)
        -- Update the HUD if applicable
        if managers.hud then
            managers.hud:set_stored_health(self._armor_stored_health / self:_max_health())
        end
    end
end