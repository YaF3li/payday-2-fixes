-- Post-require: lib/units/equipment/sentry_gun/sentrygunbrain
-- Sentry gun will ignore SWAT turrets, prioritize other enemies over shields and hold fire if it cannot damage the shield
-- WARNING: This is likely to break when an update changes sentry logic again!

local mvec3_dir = mvector3.direction
local mvec3_dot = mvector3.dot
local math_max = math.max
local tmp_vec1 = Vector3()
local tmp_vec2 = Vector3()

function SentryGunBrain:_select_focus_attention(t)
	local current_focus = self._attention_obj
	local current_pos = self._ext_movement:m_head_pos()
	local current_fwd
	if current_focus then
		current_fwd = tmp_vec2
		mvec3_dir(current_fwd, self._ext_movement:m_head_pos(), current_focus.m_head_pos)
	else
		current_fwd = self._ext_movement:m_head_fwd()
	end
	local max_dis = self._tweak_data.DETECTION_RANGE
	local function _get_weight(attention_info)
		if not attention_info.identified then
			return
		end
		local total_weight = 1
		if attention_info.verified then
		elseif attention_info.verified_t and t - attention_info.verified_t < 3 then
			local max_duration = 3
			local elapsed_t = t - attention_info.verified_t
			total_weight = total_weight * math.lerp(1, 0.6, elapsed_t / max_duration)
		else
			return
		end
        if attention_info.settings.weight_mul then
			total_weight = total_weight * attention_info.settings.weight_mul
		end
        
        -- SWAT turrets should never be targeted
        if self:is_turret(attention_info) then return -1 end
        
        -- Shields have very low priority and should not be targeted from the front
        if self:is_shield(attention_info) then
            if self:is_shield_blocked(attention_info) then
                return -1
            else
                return 0.01
            end
        end
        
		local dis = mvec3_dir(tmp_vec1, current_pos, attention_info.m_head_pos)
		local dis_weight = math_max(0, (max_dis - dis) / max_dis)
		total_weight = total_weight * dis_weight
		local dot_weight = 1 + mvec3_dot(tmp_vec1, current_fwd)
		dot_weight = dot_weight * dot_weight * dot_weight
		total_weight = total_weight * dot_weight
		return total_weight
	end

	local best_focus_attention, best_focus_weight
	local best_focus_reaction = 0
	for u_key, attention_info in pairs(self._detected_attention_objects) do
		local weight = _get_weight(attention_info)
		if weight and (best_focus_reaction < attention_info.reaction or best_focus_reaction == attention_info.reaction and (not best_focus_weight or best_focus_weight < weight)) then
			best_focus_weight = weight
			best_focus_attention = attention_info
			best_focus_reaction = attention_info.reaction
		end
	end
    
    if best_focus_weight and best_focus_weight < 0 then
        best_focus_reaction = AIAttentionObject.REACT_AIM
    end
    
	if current_focus ~= best_focus_attention then
		if best_focus_attention then
			local attention_data = {
				unit = best_focus_attention.unit,
				u_key = best_focus_attention.u_key,
				handler = best_focus_attention.handler,
				-- reaction = best_focus_attention.reaction
                reaction = best_focus_reaction
			}
			self._ext_movement:set_attention(attention_data)
		else
			self._ext_movement:set_attention()
		end
		self._attention_obj = best_focus_attention
	elseif best_focus_attention and self._ext_movement:attention() and best_focus_reaction ~= self._ext_movement:attention().reaction then
        local attention_data = {
			unit = best_focus_attention.unit,
			u_key = best_focus_attention.u_key,
			handler = best_focus_attention.handler,
			reaction = best_focus_reaction
		}
		self._ext_movement:set_attention(attention_data)
    end
end

function SentryGunBrain:is_shield(attention)
    local base = attention.unit:base()
    if not base then return false end
    return base._tweak_table and base._tweak_table == "shield"
end

function SentryGunBrain:is_turret(attention)
    local base = attention.unit:base()
    if not base then return false end
    return base._tweak_table_id and base._tweak_table_id == "swat_van_turret_module"
end

function SentryGunBrain:is_shield_blocked(attention)
    if not attention.unit:movement() then return false end
    local col_ray = World:raycast("ray", self._ext_movement:m_head_pos(), attention.unit:movement():m_com(), "slot_mask", self._unit:weapon()._bullet_slotmask, "ignore_unit", self._unit:weapon()._setup.ignore_units)
    return col_ray and col_ray.unit and col_ray.unit:in_slot(8)
end