-- Post-require: lib/units/weapons/trip_mine/tripminebase
-- In stealth, placed trip mines will automatically be in sensor mode / turned off (if player has the skill)

local _TripMineBase_setup = TripMineBase.setup

function TripMineBase:setup(sensor_upgrade)
	_TripMineBase_setup(self, sensor_upgrade)
	if managers.player:has_category_upgrade("trip_mine", "can_switch_on_off") or managers.player:has_category_upgrade("trip_mine", "sensor_toggle") then
	    self._startup_armed = not managers.groupai:state():whisper_mode()
    end
end