-- Post-require: lib/player_actions/skills/playeractionunseenstrike
-- Fix for Unseen Strike stopping when you take damage
-- NOTE: I have no idea how this was intended to work, so I just made it keep running until the time is up
-- NOTE: Due to the way this whole thing is coded, with this fix, you cannot trigger the effect while it is active

function PlayerAction.UnseenStrikeStart.Function(player_manager, max_duration, crit_chance)
    local co = coroutine.running()
    local current_time = Application:time()
    local target_time = Application:time() + max_duration

    player_manager:add_to_crit_mul(crit_chance - 1)
    
    while current_time <= target_time do
        coroutine.yield(co)
        current_time = Application:time()
    end
    
    player_manager:sub_from_crit_mul(crit_chance - 1)
end