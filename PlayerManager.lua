-- Post-require: lib/managers/playermanager
-- Possible Fix for crash when switching from skill set with Unseen Strike, Shock And Awe, Desperado or Bloodthirst to one without

-- ----
local _PlayerManager_upgrade_value = PlayerManager.upgrade_value

function PlayerManager:upgrade_value(category, upgrade, default)
    local result = _PlayerManager_upgrade_value(self, category, upgrade, default)
    
    if type(result) == "number" then
        if category == "player" or category == "pistol" then
            if upgrade == "automatic_faster_reload"
            or upgrade == "stacked_accuracy_bonus"
            or upgrade == "melee_damage_stacking"
            or upgrade == "unseen_increased_crit_chance" then
                return nil
            end
        end
    end
    
    return result
end