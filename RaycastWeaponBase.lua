-- Post-require: lib/units/weapons/raycastweaponbase
-- Fix for custom shotgun ammo and DMR kits not getting benefits from WiC and FL Aced
-- (I assume this is a bug or at least very bad design, if you disagree, just don't use this)
-- U101: Not sure if this is still needed, I think some special ammo types still have a reduction

function RaycastWeaponBase:add_ammo(ratio, add_amount_override)
    if self:ammo_max() then
        return false, 0
    end
    
    local multiplier_min = managers.player:upgrade_value("player", "pick_up_ammo_multiplier", 1)
    multiplier_min = multiplier_min + (managers.player:upgrade_value("player", "pick_up_ammo_multiplier_2", 1) - 1)
    if self._ammo_data and self._ammo_data.ammo_pickup_min_mul then
        multiplier_min = multiplier_min * self._ammo_data.ammo_pickup_min_mul
    end
    
    local multiplier_max = managers.player:upgrade_value("player", "pick_up_ammo_multiplier", 1)
    multiplier_max = multiplier_max + (managers.player:upgrade_value("player", "pick_up_ammo_multiplier_2", 1) - 1)
    if self._ammo_data and self._ammo_data.ammo_pickup_max_mul then
        multiplier_max = multiplier_max * self._ammo_data.ammo_pickup_max_mul
    end
    
    local add_amount = add_amount_override
    local picked_up = true
    if not add_amount then
        local rng_ammo = math.lerp(self._ammo_pickup[1] * multiplier_min, self._ammo_pickup[2] * multiplier_max, math.random())
        picked_up = rng_ammo > 0
        add_amount = math.max(0, math.round(rng_ammo))
    end
    add_amount = math.floor(add_amount * (ratio or 1))
    self:set_ammo_total(math.clamp(self:get_ammo_total() + add_amount, 0, self:get_ammo_max()))
    if Application:production_build() then
        managers.player:add_weapon_ammo_gain(self._name_id, add_amount)
    end
    return picked_up, add_amount
end