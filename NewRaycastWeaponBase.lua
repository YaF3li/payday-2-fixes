-- Post-require: lib/units/weapons/newraycastweaponbase
-- My attempt to fix the broken weapon accuracy as of U104
-- It's probably far from being good, but at least you don't get 0 spread when ADSing with a low accuracy shotgun

function NewRaycastWeaponBase:conditional_accuracy_multiplier(current_state)
    local mul = 1
    if not current_state then
        return mul
    end
    local pm = managers.player
    if current_state:in_steelsight() and self:is_single_shot() then
        mul = mul + (1 - pm:upgrade_value("player", "single_shot_accuracy_inc", 1))
    end
    if current_state:in_steelsight() and self:is_category("shotgun") then
        mul = mul + (1 - pm:upgrade_value("shotgun", "steelsight_accuracy_inc", 1))
    end
    if current_state._moving then
        mul = mul + (1 - pm:upgrade_value("player", "weapon_movement_stability", 1))
    end
    mul = mul + (1 - pm:get_property("desperado", 1))
    return self:_convert_add_to_mul(mul)
end

function NewRaycastWeaponBase:_get_spread(user_unit)
    local current_state = user_unit:movement()._current_state
    if not current_state then
        return 0
    end
    local spread_index = self._current_stats_indices and self._current_stats_indices.spread or 1
    local cond_spread_addend = self:conditional_accuracy_addend(current_state)
    local spread_multiplier = 1
    spread_multiplier = spread_multiplier - (1 - self:spread_multiplier(current_state))
    spread_multiplier = spread_multiplier - (1 - self:conditional_accuracy_multiplier(current_state))
    spread_multiplier = self:_convert_add_to_mul(spread_multiplier)
    local spread_addend = self:spread_index_addend(current_state) + cond_spread_addend
    spread_index = math.ceil((spread_index + spread_addend) * spread_multiplier)
    spread_index = math.clamp(spread_index, 1, #tweak_data.weapon.stats.spread)
    local spread = tweak_data.weapon.stats.spread[spread_index]
    local stance_mul = 1
    if current_state:in_steelsight() then
        stance_mul = tweak_data.weapon[self._name_id].spread[current_state._moving and "moving_steelsight" or "steelsight"]
    elseif current_state._state_data.ducking then
        stance_mul = tweak_data.weapon[self._name_id].spread[current_state._moving and "moving_crouching" or "crouching"]
    else
        stance_mul = tweak_data.weapon[self._name_id].spread[current_state._moving and "moving_standing" or "standing"]
    end
    spread = spread * stance_mul
    return math.max(spread, 0)
end