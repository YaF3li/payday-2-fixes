-- Post-require: lib/units/player_team/logics/teamailogicidle
-- Fix for a (host-side) crash that (rarely) happens upon completion of a day / heist
-- It is hard to test this fix for obvious reasons, but it should work

local _TeamAILogicIdle_on_alert = TeamAILogicIdle.on_alert

function TeamAILogicIdle.on_alert(data, alert_data)
    local alert_unit = alert_data[5]
    if alert_unit then
        _TeamAILogicIdle_on_alert(data, alert_data)
    end
end