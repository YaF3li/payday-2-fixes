-- Post-require: lib/units/equipment/ammo_bag/ammobagbase
-- EXPERIMENTAL: Fix for ammo bags sometimes not syncing their empty state correctly to all players
-- It should prevent the smoke that doesn't despawn glitch, but all players need this installed
-- And yes, this issue seems to persist as of U95.3, probably more rare than before, but I had it happen the other day
-- So this is my attempt to fix it, currently not tested very much... I hope it works :)
-- UPDATE to U101, not sure if this bug still persists, but in any case, this works with U101

function AmmoBagBase:take_ammo(unit)
    if self._empty then
        return false, false
    end
    
    local taken = self:_take_ammo(unit)
    
    if taken > 0 then
        unit:sound():play("pickup_ammo")
    end
    
    if 0 >= self._ammo_amount then
        managers.network:session():send_to_peers_synched("sync_ammo_bag_ammo_taken", self._unit, self._max_ammo_amount)
        self:_set_empty()
    else
        managers.network:session():send_to_peers_synched("sync_ammo_bag_ammo_taken", self._unit, taken)
        self:_set_visual_stage()
    end
    
    local bullet_storm = false
    if self._bullet_storm_level and 0 < self._bullet_storm_level then
        bullet_storm = self._BULLET_STORM[self._bullet_storm_level] * taken
    end
    return taken > 0, bullet_storm
end