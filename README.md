# PAYDAY 2 Fixes
Fixes for bugs, annoyances and other stuff... or something.
These scripts might not be tested thoroughly, things may fail, just be aware of that.
Also, I can't test them with every update, so some might be outdated even though they're not listed as such.

### Quick breakdown of the scripts:
* ElementAreaTrigger: Ilija won't kill Jokers
* TripMineBase: Trip mines start in sensor mode when placed during stealth
* RaycastWeaponBase: Custom shotgun ammo and DMR kits make use of WiC and/or FL Aced
* MenuManagerPD2: Equips a random mask every time you enter a lobby
* SentryGunDamage: Fix for player sentry guns' damage calculation
* TeamAILogicIdle: Fixes one bug that might (rarely) cause host to crash on completion of a day
* PlayerDamage: Ex-Presidents will not discard unused stored health upon healing
* AmmoBagBase: Fix for ammo bags not syncing their empty state
* CopDamage: Fix for Body Expertise damage calculation (you may not want to use this)
* PlayerActionUnseenStrike: Fix for Unseen Strike's reset on damage
* PlayerManager: Fix for a crash on skill set switch
* SkillTreeTweakData: Fix for Martial Arts Aced not working
* NewRaycastWeaponBase: (Partial) fix for weapon accuracy calculations

### Outdated scripts (but probably not fixed):
* SentryGunBrain: Sentries will not target SWAT turrets and shields only if they can damage them

### Obsolete scripts (now fixed by Overkill):
* UpgradesTweakData: Sharpshooter Aced Fix (Gimme dat +8 Stb)
* NewShotgunBase: Fix for AP slugs and DB rounds not piercing shields reliably
* PlayerStandard: Fix for shouting at converted enemies
* BlackMarketManager: Fix for Marksman Aced accuracy calculations