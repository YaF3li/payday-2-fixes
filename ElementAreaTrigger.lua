-- Post-require: lib/managers/mission/elementareatrigger
-- Fix for buyable sniper asset sometimes killing converted cops
-- U101: Fix for possible rare crash

local _ElementAreaTrigger_project_instigators = ElementAreaTrigger.project_instigators

function ElementAreaTrigger:project_instigators()
    local instigators = _ElementAreaTrigger_project_instigators(self)
    if self._values.instigator == "enemies" and managers.groupai:state():police_hostage_count() > 0 then
        for i = 1, #instigators do
            local movement = instigators[i] and instigators[i]:movement()
            local team_data = movement and movement:team()
            if team_data and team_data.id == "converted_enemy" then
                table.remove(instigators, i)
            end
        end
    end
    return instigators
end